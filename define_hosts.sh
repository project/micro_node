#!/bin/bash

declare -a HOSTS=(${CONTAINER_NAME} 'microsite.local')
declare -a DOMAINS=('one.local' 'two.local' 'three.local' 'four.local' 'five.local')
declare -a SUBDOMAINS=('www' 'one' 'two' 'three' 'four' 'five')

for DOMAIN in ${DOMAINS[@]}; do
    echo '127.0.0.1' ${DOMAIN} >> /etc/hosts
done
for HOST in ${HOSTS[@]}; do
    echo '127.0.0.1' ${HOST} >> /etc/hosts

    for SUBDOMAIN in ${SUBDOMAINS[@]}; do
      echo '127.0.0.1' ${SUBDOMAIN}.${HOST} >> /etc/hosts
      echo '127.0.0.1' ${SUBDOMAIN}.example.${HOST} >> /etc/hosts
    done
done
