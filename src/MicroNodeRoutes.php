<?php

namespace Drupal\micro_node;

use Drupal\node\Entity\NodeType;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class MicroNodeRoutes {

  const ROUTE_NAME = "micro_node.node_add";

  public function routes() {
    $node_types = \Drupal::config('micro_node.settings')->get('node_types');
    $routeCollection = new RouteCollection();
    if (empty($node_types)) {
      return $routeCollection;
    }

    $defaults = [
      '_entity_form' => 'node.default',
      '_title_callback' => '\Drupal\node\Controller\NodeController::addPageTitle',
    ];
    $requirements = [
      'site' => '\d+',
      '_custom_access' => '\Drupal\micro_node\Access\NodeAddAccess:access',
    ];
    $options = [
      '_node_operation_route' => TRUE,
      '_admin_route' => TRUE,
      'parameters' => [
        'site' => [
          'type' => 'entity:site',
          'with_config_overrides' => TRUE,
        ],
        'node_type' => [
          'type' => 'entity:node_type',
          'with_config_overrides' => TRUE,
        ],
      ],
    ];

    $routeCollection = new RouteCollection();
    $routeCollection->add(static::ROUTE_NAME, new Route("/site/{site}/add/{node_type}", $defaults, $requirements, $options));
    return $routeCollection;
  }

}
