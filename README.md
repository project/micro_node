# Micro Node

This module integrates node entities with a [micro site](https://www.drupal.org/project/micro_site).

For a full description of the module, visit the
[project page](https://www.drupal.org/project/micro_node).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/micro_node).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [micro site](https://www.drupal.org/project/micro_site).


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- It is highly recommended to not use user 1 when managing micro sites with
 nodes, or any user with the permission "bypass node access". Otherwise, these
 users will see any nodes published on any micro sites. You should create at
 least another admin like role, role which do not have this permission.


## Maintainers

- Flocon de toile -[flocondetoile](https://drupal.org/u/flocondetoile)
