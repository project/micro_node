<?php

namespace Drupal\Tests\micro_node\Traits;

use Drupal\Core\Session\AccountInterface;
use Drupal\micro_node\MicroNodeFields;

/**
 * Contains helper classes for tests to set up various configuration.
 */
trait MicroNodeTestTrait {

  /**
   * Configure the entity form display.
   *
   * @param $entity_type
   * @param $bundle
   * @param string $mode
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function configureEntityFormDisplay($entity_type, $bundle, $mode = 'default') {
    // Ensure that form display is well configured.
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $entity_form_display = $display_repository->getFormDisplay($entity_type, $bundle, $mode);
    $options = [
      'type' => 'entity_reference_autocomplete',
      'weight' => 40,
      'region' => 'content',
    ];
    $entity_form_display->setComponent('field_sites', $options)->save();
    $options = [
      'type' => 'boolean_checkbox',
      'settings' => ['display_label' => 1],
      'weight' => 41,
      'region' => 'content',
    ];
    $entity_form_display->setComponent(MicroNodeFields::NODE_SITES_ALL, $options)
      ->setComponent(MicroNodeFields::NODE_SITES_PUBLISH_MASTER, $options)
      ->setComponent(MicroNodeFields::NODE_SITES_DISABLE_CANONICAL_URL, $options)
      ->save();
  }
}
