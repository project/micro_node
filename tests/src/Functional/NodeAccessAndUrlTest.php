<?php

namespace Drupal\Tests\micro_node\Functional;

use Drupal\Core\Url;
use Drupal\micro_node\MicroNodeFields;
use Drupal\Tests\micro_site\Functional\MicroSiteBase;

/**
 * Test the access node and url generated for Micro Node module.
 *
 * @group micro_node
 */
class NodeAccessAndUrlTest extends MicroSiteBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['micro_site', 'micro_node', 'micro_node_test'];

  /**
   * A simple user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The theme to install as the default for testing.
   *
   * Defaults to the install profile's default theme, if it specifies any.
   *
   * @var string
   */
  protected $defaultTheme = 'olivero';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->configFactory->getEditable('micro_node.settings')->set('node_types', ['article', 'page'])->save(TRUE);
    foreach (['article', 'page'] as $bundle) {
      micro_node_assign_fields('node', $bundle);
    }
    drupal_flush_all_caches();
  }

  /**
   * Tests the access records.
   */
  public function testAccess() {
    $this->drupalGet(Url::fromUri($this->masterUrl));
    $this->assertSession()->statusCodeEquals(200);

    $generic = $this->createSiteType('generic', 'Generic', 'Generic site type');
    $settings = ['user_id' => $this->microSiteOwnerUser->id(), 'registered' => TRUE, 'status' => TRUE];
    $site_one = $this->createSite('generic', 'Site One', 'Site one slogan', 'domain', 'one.microsite.local', 'siteone@microsite.local', $settings);

    $settings = ['user_id' => $this->microSiteOwnerUser->id(), 'registered' => TRUE, 'status' => TRUE];
    $site_two = $this->createSite('generic', 'Site Two', 'Site two slogan', 'domain', 'two.microsite.local', 'sitetwo@microsite.local', $settings);
    $node_storage = $this->entityTypeManager->getStorage('node');

    $node1 = $this->drupalCreateNode([
      'title' => 'Article 1 master',
      'type' => 'article',
    ]);
    $this->assertNotNull($node_storage->load($node1->id()), 'Article 1 created.');
    $node2 = $this->drupalCreateNode([
      'title' => 'Article 2 master and site one',
      'type' => 'article',
      MicroNodeFields::NODE_SITES => [$site_one->id()],
    ]);
    $this->assertNotNull($node_storage->load($node2->id()), 'Article 2 created.');

    $node3 = $this->drupalCreateNode([
      'title' => 'Article 3 master and all sites',
      'type' => 'article',
      MicroNodeFields::NODE_SITES_ALL => TRUE,
    ]);
    $this->assertNotNull($node_storage->load($node3->id()), 'Article 3 created.');
    $node4 = $this->drupalCreateNode([
      'title' => 'Article 4 site one',
      'type' => 'article',
      'site_id' => $site_one->id(),
    ]);
    $this->assertNotNull($node_storage->load($node4->id()), 'Article 4 created.');
    $node5 = $this->drupalCreateNode([
      'title' => 'Article 5 site one and two',
      'type' => 'article',
      'site_id' => $site_one->id(),
      MicroNodeFields::NODE_SITES => [$site_two->id()],
    ]);
    $this->assertNotNull($node_storage->load($node5->id()), 'Article 5 created.');
    $node6 = $this->drupalCreateNode([
      'title' => 'Article 6 site two',
      'type' => 'article',
      'site_id' => $site_two->id(),
    ]);
    $this->assertNotNull($node_storage->load($node6->id()), 'Article 6 created.');
    node_access_rebuild();

    $views_url = '/micro-node-content';
    // Check on master.
    $this->drupalGet($this->masterUrl . $views_url);
    $this->assertSession()->statusCodeEquals(200);
    $xpath = $this->xpath('//article[contains(@class, "node--type-article")]');
    $this->assertEquals(count($xpath), 3, '3 articles found on master.');
    $this->assertSession()->pageTextContains('Article 1 master');
    $this->assertSession()->pageTextContains('Article 2 master and site one');
    $this->assertSession()->pageTextContains('Article 3 master and all sites');
    $this->assertSession()->pageTextNotContains('Article 4 site one');
    $this->assertSession()->pageTextNotContains('Article 5 site one and two');
    $this->assertSession()->pageTextNotContains('Article 6 site two');
    // Check URLs
    $xpath = $this->xpath("//h2/a[@href='/node/" . $node1->id() . "']");
    $this->assertEquals(count($xpath), 1, 'Correct URL found.');
    $xpath = $this->xpath("//h2/a[@href='/node/" . $node2->id() . "']");
    $this->assertEquals(count($xpath), 1, 'Correct URL found.');
    $xpath = $this->xpath("//h2/a[@href='/node/" . $node3->id() . "']");
    $this->assertEquals(count($xpath), 1, 'Correct URL found.');
    // Click response.
    $this->clickLink($node1->label());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($this->masterUrl . $views_url);
    $this->clickLink($node2->label());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($this->masterUrl . $views_url);
    $this->clickLink($node3->label());
    $this->assertSession()->statusCodeEquals(200);

    // Check on site one.
    $this->drupalGet($site_one->getSitePath() . $views_url);
    $this->assertSession()->statusCodeEquals(200);
    $xpath = $this->xpath('//article[contains(@class, "node--type-article")]');
    $this->assertEquals(count($xpath), 4, '4 articles found on site one.');
    $this->assertSession()->pageTextNotContains('Article 1 master');
    $this->assertSession()->pageTextContains('Article 2 master and site one');
    $this->assertSession()->pageTextContains('Article 3 master and all sites');
    $this->assertSession()->pageTextContains('Article 4 site one');
    $this->assertSession()->pageTextContains('Article 5 site one and two');
    $this->assertSession()->pageTextNotContains('Article 6 site two');
    // Check URLs
    $xpath = $this->xpath("//h2/a[@href='" . $this->masterUrl . "/node/" . $node2->id() . "']");
    $this->assertEquals(count($xpath), 1, 'Correct URL found.');
    $xpath = $this->xpath("//h2/a[@href='" . $this->masterUrl . "/node/" . $node3->id() . "']");
    $this->assertEquals(count($xpath), 1, 'Correct URL found.');
    $xpath = $this->xpath("//h2/a[@href='" . $site_one->getSitePath() . "/node/" . $node4->id() . "']");
    $this->assertEquals(count($xpath), 1, 'Correct URL found.');
    $xpath = $this->xpath("//h2/a[@href='" . $site_one->getSitePath() . "/node/" . $node5->id() . "']");
    $this->assertEquals(count($xpath), 1, 'Correct URL found.');
    // Click response.
    $this->clickLink($node2->label());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($site_one->getSitePath() . $views_url);
    $this->clickLink($node3->label());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($site_one->getSitePath() . $views_url);
    $this->clickLink($node4->label());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($site_one->getSitePath() . $views_url);
    $this->clickLink($node5->label());
    $this->assertSession()->statusCodeEquals(200);

    // Check on site two.
    $this->drupalGet($site_two->getSitePath() . $views_url);
    $this->assertSession()->statusCodeEquals(200);
    $xpath = $this->xpath('//article[contains(@class, "node--type-article")]');
    $this->assertEquals(count($xpath), 3, '3 articles found on site two.');
    $this->assertSession()->pageTextNotContains('Article 1 master');
    $this->assertSession()->pageTextNotContains('Article 2 master and site one');
    $this->assertSession()->pageTextContains('Article 3 master and all sites');
    $this->assertSession()->pageTextNotContains('Article 4 site one');
    $this->assertSession()->pageTextContains('Article 5 site one and two');
    $this->assertSession()->pageTextContains('Article 6 site two');
    // Check URLs
    $xpath = $this->xpath("//h2/a[@href='" . $this->masterUrl . "/node/" . $node3->id() . "']");
    $this->assertEquals(count($xpath), 1, 'Correct URL found.');
    $xpath = $this->xpath("//h2/a[@href='" . $site_one->getSitePath() . "/node/" . $node5->id() . "']");
    $this->assertEquals(count($xpath), 1, 'Correct URL found.');
    $xpath = $this->xpath("//h2/a[@href='" . $site_two->getSitePath() . "/node/" . $node6->id() . "']");
    $this->assertEquals(count($xpath), 1, 'Correct URL found.');
    // Click response.
    $this->clickLink($node3->label());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($site_two->getSitePath() . $views_url);
    $this->clickLink($node5->label());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($site_two->getSitePath() . $views_url);
    $this->clickLink($node6->label());
    $this->assertSession()->statusCodeEquals(200);

    // Test publish on master.
    $node6->set(MicroNodeFields::NODE_SITES_PUBLISH_MASTER, TRUE);
    $node6->save();

    $this->drupalGet($this->masterUrl . $views_url);
    $this->assertSession()->statusCodeEquals(200);
    $xpath = $this->xpath('//article[contains(@class, "node--type-article")]');
    $this->assertEquals(count($xpath), 4, '4 articles found on master.');
    $this->assertSession()->pageTextContains('Article 6 site two');
    // Check URLs
    $xpath = $this->xpath("//h2/a[@href='" . $site_two->getSitePath() . "/node/" . $node6->id() . "']");
    $this->assertEquals(count($xpath), 1, 'Correct URL found.');
    // Click response.
    $this->clickLink($node6->label());
    $this->assertSession()->statusCodeEquals(200);
  }

}
