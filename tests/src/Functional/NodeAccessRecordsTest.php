<?php

namespace Drupal\Tests\micro_node\Functional;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\micro_node\MicroNodeFields;
use Drupal\micro_site\SiteUsers;
use Drupal\Tests\micro_site\Functional\MicroSiteBase;

/**
 * Test the access records for Micro Node module.
 *
 * @group micro_node
 */
class NodeAccessRecordsTest extends MicroSiteBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['micro_site', 'micro_node'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The theme to install as the default for testing.
   *
   * Defaults to the install profile's default theme, if it specifies any.
   *
   * @var string
   */
  protected $defaultTheme = 'olivero';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->configFactory->getEditable('micro_node.settings')->set('node_types', ['article', 'page'])->save(TRUE);
    foreach (['article', 'page'] as $bundle) {
      micro_node_assign_fields('node', $bundle);
    }
    drupal_flush_all_caches();
  }

  /**
   * Tests the access records.
   */
  public function testAccessRecords() {
    $this->drupalGet(Url::fromUri($this->masterUrl));
    $this->assertSession()->statusCodeEquals(200);

    $generic = $this->createSiteType('generic', 'Generic', 'Generic site type');
    $settings = ['user_id' => $this->microSiteOwnerUser->id(), 'registered' => TRUE, 'status' => TRUE];
    $site_one = $this->createSite('generic', 'Site One', 'Site one slogan', 'domain', 'one.microsite.local', 'siteone@microsite.local', $settings);

    $settings = ['user_id' => $this->microSiteOwnerUser->id(), 'registered' => TRUE];
    $site_two = $this->createSite('generic', 'Site Two', 'Site two slogan', 'domain', 'two.microsite.local', 'sitetwo@microsite.local', $settings);
    $node_storage = $this->entityTypeManager->getStorage('node');

    // Create an article node.
    $node1 = $this->drupalCreateNode([
      'type' => 'article',
      'site_id' => [$site_one->id()],
    ]);
    $this->assertNotNull($node_storage->load($node1->id()), 'Article node 1 created.');

    // Check the grants added by micro_node_node_access_records.
    $query = 'SELECT realm, gid, grant_view, grant_update, grant_delete FROM {node_access} WHERE nid = :nid ORDER BY realm ASC';
    $records = Database::getConnection()
      ->query($query, [':nid' => $node1->id()])
      ->fetchAll();

    $this->assertCount(2, $records, 'Returned the correct number of rows.');
    $this->assertEquals($records[0]->realm, 'site_id', 'Grant with site_id acquired for node.');
    $this->assertEquals($records[0]->gid, $site_one->id(), 'Grant with proper id acquired for node.');
    $this->assertEquals($records[0]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[0]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[0]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[1]->realm, 'site_node_owner', 'Grant with site_node_owner acquired for node.');
    $this->assertEquals($records[1]->gid, $node1->getOwnerId(), 'Grant with proper id acquired for node.');
    $this->assertEquals($records[1]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[1]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[1]->grant_delete, 1, 'Grant delete stored.');

    $node2 = $this->drupalCreateNode([
      'type' => 'article',
      'site_id' => [$site_one->id()],
      MicroNodeFields::NODE_SITES => [$site_two->id()],
    ]);
    $this->assertNotNull($node_storage->load($node2->id()), 'Article node 2 created.');

    // Check the grants added by micro_node_node_access_records.
    $query = 'SELECT realm, gid, grant_view, grant_update, grant_delete FROM {node_access} WHERE nid = :nid ORDER BY realm ASC, gid ASC';
    $records = Database::getConnection()
      ->query($query, [':nid' => $node2->id()])
      ->fetchAll();

    $this->assertCount(3, $records, 'Returned the correct number of rows.');
    $this->assertEquals($records[0]->realm, 'site_id', 'Grant with site_id acquired for node.');
    $this->assertEquals($records[0]->gid, $site_one->id(), 'Grant with proper id acquired for node.');
    $this->assertEquals($records[0]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[0]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[0]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[1]->realm, 'site_id', 'Grant with site_id acquired for node.');
    $this->assertEquals($records[1]->gid, $site_two->id(), 'Grant with proper id acquired for node.');
    $this->assertEquals($records[1]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[1]->grant_update, 0, 'Grant update stored.');
    $this->assertEquals($records[1]->grant_delete, 0, 'Grant delete stored.');
    $this->assertEquals($records[2]->realm, 'site_node_owner', 'Grant with site_node_owner acquired for node.');
    $this->assertEquals($records[2]->gid, $node2->getOwnerId(), 'Grant with proper id acquired for node.');
    $this->assertEquals($records[2]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[2]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[2]->grant_delete, 1, 'Grant delete stored.');

    // On master
    $node3 = $this->drupalCreateNode([
      'type' => 'article',
    ]);
    $this->assertNotNull($node_storage->load($node3->id()), 'Article node 3 created.');

    // Check the grants added by micro_node_node_access_records.
    $query = 'SELECT realm, gid, grant_view, grant_update, grant_delete FROM {node_access} WHERE nid = :nid';
    $records = Database::getConnection()
      ->query($query, [':nid' => $node3->id()])
      ->fetchAll();

    $this->assertCount(4, $records, 'Returned the correct number of rows.');
    $this->assertEquals($records[0]->realm, 'master', 'Grant with master acquired for node.');
    $this->assertEquals($records[0]->gid, 0, 'Grant with proper id acquired for node.');
    $this->assertEquals($records[0]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[0]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[0]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[1]->realm, 'master_article', 'Grant with master_article acquired for node.');
    $this->assertEquals($records[1]->gid, 0, 'Grant with proper id acquired for node.');
    $this->assertEquals($records[1]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[1]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[1]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[2]->realm, 'master_own_article', 'Grant with master_own_article acquired for node.');
    $this->assertEquals($records[2]->gid, $node3->getOwnerId(), 'Grant with proper id acquired for node.');
    $this->assertEquals($records[2]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[2]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[2]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[3]->realm, 'master_own_unpublished', 'Grant with master_own_unpublished acquired for node.');
    $this->assertEquals($records[3]->gid, $node3->getOwnerId(), 'Grant with proper id acquired for node.');
    $this->assertEquals($records[3]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[3]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[3]->grant_delete, 1, 'Grant delete stored.');

    // On master and all sites
    $node4 = $this->drupalCreateNode([
      'type' => 'article',
      MicroNodeFields::NODE_SITES_ALL => TRUE,
    ]);
    $this->assertNotNull($node_storage->load($node4->id()), 'Article node 3 created.');

    // Check the grants added by micro_node_node_access_records.
    $query = 'SELECT realm, gid, grant_view, grant_update, grant_delete FROM {node_access} WHERE nid = :nid';
    $records = Database::getConnection()
      ->query($query, [':nid' => $node4->id()])
      ->fetchAll();

    $this->assertCount(5, $records, 'Returned the correct number of rows.');
    $this->assertEquals($records[0]->realm, 'master', 'Grant with master acquired for node.');
    $this->assertEquals($records[0]->gid, 0, 'Grant with proper id acquired for node.');
    $this->assertEquals($records[0]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[0]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[0]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[1]->realm, 'master_article', 'Grant with master_article acquired for node.');
    $this->assertEquals($records[1]->gid, 0, 'Grant with proper id acquired for node.');
    $this->assertEquals($records[1]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[1]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[1]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[2]->realm, 'master_own_article', 'Grant with master_own_article acquired for node.');
    $this->assertEquals($records[2]->gid, $node4->getOwnerId(), 'Grant with proper id acquired for node.');
    $this->assertEquals($records[2]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[2]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[2]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[3]->realm, 'master_own_unpublished', 'Grant with master_own_unpublished acquired for node.');
    $this->assertEquals($records[3]->gid, $node4->getOwnerId(), 'Grant with proper id acquired for node.');
    $this->assertEquals($records[3]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[3]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[3]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[4]->realm, 'site_all', 'Grant with site_all acquired for node.');
    $this->assertEquals($records[4]->gid, 0, 'Grant with proper id acquired for node.');
    $this->assertEquals($records[4]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[4]->grant_update, 0, 'Grant update stored.');
    $this->assertEquals($records[4]->grant_delete, 0, 'Grant delete stored.');

    // Create an article node on site one and publish on master.
    $node5 = $this->drupalCreateNode([
      'type' => 'article',
      'site_id' => [$site_one->id()],
      MicroNodeFields::NODE_SITES_PUBLISH_MASTER => TRUE,
    ]);
    $this->assertNotNull($node_storage->load($node5->id()), 'Article node 5 created.');

    // Check the grants added by micro_node_node_access_records.
    $query = 'SELECT realm, gid, grant_view, grant_update, grant_delete FROM {node_access} WHERE nid = :nid ORDER BY realm ASC';
    $records = Database::getConnection()
      ->query($query, [':nid' => $node5->id()])
      ->fetchAll();

    $this->assertCount(6, $records, 'Returned the correct number of rows.');

    $this->assertEquals($records[0]->realm, 'master', 'Grant with master acquired for node.');
    $this->assertEquals($records[0]->gid, 0, 'Grant with proper id acquired for node.');
    $this->assertEquals($records[0]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[0]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[0]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[1]->realm, 'master_article', 'Grant with master_article acquired for node.');
    $this->assertEquals($records[1]->gid, 0, 'Grant with proper id acquired for node.');
    $this->assertEquals($records[1]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[1]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[1]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[2]->realm, 'master_own_article', 'Grant with master_own_article acquired for node.');
    $this->assertEquals($records[2]->gid, $node5->getOwnerId(), 'Grant with proper id acquired for node.');
    $this->assertEquals($records[2]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[2]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[2]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[3]->realm, 'master_own_unpublished', 'Grant with master_own_unpublished acquired for node.');
    $this->assertEquals($records[3]->gid, $node5->getOwnerId(), 'Grant with proper id acquired for node.');
    $this->assertEquals($records[3]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[3]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[3]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[4]->realm, 'site_id', 'Grant with site_id acquired for node.');
    $this->assertEquals($records[4]->gid, $site_one->id(), 'Grant with proper id acquired for node.');
    $this->assertEquals($records[4]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[4]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[4]->grant_delete, 1, 'Grant delete stored.');
    $this->assertEquals($records[5]->realm, 'site_node_owner', 'Grant with site_node_owner acquired for node.');
    $this->assertEquals($records[5]->gid, $node5->getOwnerId(), 'Grant with proper id acquired for node.');
    $this->assertEquals($records[5]->grant_view, 1, 'Grant view stored.');
    $this->assertEquals($records[5]->grant_update, 1, 'Grant update stored.');
    $this->assertEquals($records[5]->grant_delete, 1, 'Grant delete stored.');
  }



}
