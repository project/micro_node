<?php

namespace Drupal\Tests\micro_node\Functional;

use Drupal\Core\Url;
use Drupal\micro_site\SiteUsers;
use Drupal\node\Entity\Node;
use Drupal\Tests\micro_node\Traits\MicroNodeTestTrait;
use Drupal\Tests\micro_site\Functional\MicroSiteBase;

/**
 * Test for Micro Node module.
 *
 * @group micro_node
 */
class NodeEditRedirectTest extends MicroSiteBase {

  use MicroNodeTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['micro_site', 'micro_node'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The theme to install as the default for testing.
   *
   * Defaults to the install profile's default theme, if it specifies any.
   *
   * @var string
   */
  protected $defaultTheme = 'olivero';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->configFactory->getEditable('micro_node.settings')->set('node_types', ['article', 'page'])->save(TRUE);
    foreach (['article', 'page'] as $bundle) {
      micro_node_assign_fields('node', $bundle);
    }
    drupal_flush_all_caches();
  }

  /**
   * Tests the configuration form.
   */
  public function testNodeAccess() {
    $this->drupalGet(Url::fromUri($this->masterUrl));
    $this->assertSession()->statusCodeEquals(200);

    $generic = $this->createSiteType('generic', 'Generic', 'Generic site type', FALSE, FALSE, ['page', 'article']);
    $settings = ['user_id' => $this->microSiteOwnerUser->id(), 'registered' => TRUE, 'status' => TRUE];
    $site_one = $this->createSite('generic', 'Site One', 'Site one slogan', 'domain', 'one.microsite.local', 'siteone@microsite.local', $settings);
    $site_two = $this->createSite('generic', 'Site Two', 'Site two slogan', 'domain', 'two.microsite.local', 'sitetwo@microsite.local', $settings);
    node_access_rebuild();
    $permissions = [
      'administer nodes',
      'create article content',
      'edit any article content',
      'view published site entities',
      'publish on any assigned site',
      'access content',
    ];
    $admin = $this->createUserWithPassword($permissions);
    // Allow admin to cross publish on micro sites one and two.
    $site_one->set(SiteUsers::MICRO_SITE_MANAGER, [$admin])->save();
    $site_two->set(SiteUsers::MICRO_SITE_MANAGER, [$admin])->save();

    $this->drupalGet(Url::fromUri($this->masterUrl));
    $this->logInUser($admin);

    // Create a node on master.
    $this->drupalGet($this->masterUrl . '/node/add/article');
    $this->assertSession()->statusCodeEquals(200);
    $this->fillField('title[0][value]', 'Article master');
    $this->pressButton('Save');
    $this->assertSession()->pageTextContains('Article Article master has been created.');
    $articles = $this->entityTypeManager->getStorage('node')->loadByProperties(['title' => 'Article master']);
    $this->assertEquals(1, count($articles), 'One article found.');
    $article_master = reset($articles);
    $article_master_url = '/node/' . $article_master->id();
    $article_master_url_edit = '/node/' . $article_master->id() . '/edit';

    $this->drupalGet($this->masterUrl . $article_master_url);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($site_one->getSitePath() . $article_master_url);
    $this->assertSession()->statusCodeEquals(403);

    // Assign the node to site one.
    $target_one_id = $site_one->label() . ' (' . $site_one->id() . ')';
    $this->drupalGet($this->masterUrl . $article_master_url_edit);
    $this->fillField('site_id[0][target_id]', $target_one_id);
    $this->pressButton('Save');
    $this->assertSession()->statusCodeEquals(200);
    // Reload the article master.
    $article_master = Node::load($article_master->id());
    $new_site = $article_master->get('site_id')->referencedEntities();
    $this->assertNotEmpty($new_site);
    $new_site = reset($new_site);
    $this->assertEquals($new_site->id(), $site_one->id());

    $this->drupalGet(Url::fromUri($this->masterUrl));
    $this->clickLink('Log out');

    $this->drupalGet($this->masterUrl . $article_master_url);
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($site_one->getSitePath() . $article_master_url);
    $this->assertSession()->statusCodeEquals(200);

    // Create a node on site one.
    $this->drupalGet(Url::fromUri($site_one->getSitePath()));
    $this->assertSession()->statusCodeEquals(200);
    $this->logInUser($this->microSiteOwnerUser);
    $xpath = $this->xpath("//a[contains(@href, '/site/" . $site_one->id() . "/content')]");
    $this->assertEquals(1, count($xpath), 'Tab content not found on the micro site home.');
    $this->clickLink('Content');
    $this->assertSession()->pageTextContains('Add Article');
    $this->clickLink('Add Article');

    $this->assertSession()->fieldValueEquals('site_id[0][target_id]', $target_one_id);
    $this->fillField('title[0][value]', 'Article site one');
    $this->pressButton('Save');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Article Article site one has been created.');
    $articles = $this->entityTypeManager->getStorage('node')->loadByProperties(['title' => 'Article site one']);
    $this->assertEquals(1, count($articles), 'One article found.');
    $article = reset($articles);
    $article_url = '/node/' . $article->id();
    $article_url_edit = '/node/' . $article->id() . '/edit';

    // Assign this last node to site two.
    $target_two_id = $site_two->label() . ' (' . $site_two->id() . ')';
    $this->drupalGet($site_one->getSitePath() . $article_url_edit);
    $this->fillField('site_id[0][target_id]', $target_two_id);
    $this->pressButton('Save');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet($site_one->getSitePath() . $article_url);
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($site_two->getSitePath() . $article_url);
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Gets the permissions for a micro site owner.
   *
   * @return string[]
   *   The permissions.
   */
  protected function getMicroSiteOwnerPermissions() {
    return [
      'view own unpublished site entity',
      'view published site entities',
      'edit own site entity',
      'view micro site information',
      'publish on any assigned site',
    ];
  }

}
