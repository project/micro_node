<?php

namespace Drupal\Tests\micro_node\Functional;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;
use Drupal\micro_site\SiteUsers;
use Drupal\Tests\micro_node\Traits\MicroNodeTestTrait;
use Drupal\Tests\micro_site\Functional\MicroSiteBase;

/**
 * Test for Micro Node module.
 *
 * @group micro_node
 */
class NodeCreateTest extends MicroSiteBase {

  use MicroNodeTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['micro_site', 'micro_node'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The theme to install as the default for testing.
   *
   * Defaults to the install profile's default theme, if it specifies any.
   *
   * @var string
   */
  protected $defaultTheme = 'olivero';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
  }

  /**
   * Tests the configuration form.
   */
  public function testNodeAccess() {
    $this->drupalGet(Url::fromUri($this->masterUrl));
    $this->assertSession()->statusCodeEquals(200);

    $generic = $this->createSiteType('generic', 'Generic', 'Generic site type');
    $settings = ['user_id' => $this->microSiteOwnerUser->id()];
    $site_one = $this->createSite('generic', 'Site One', 'Site one slogan', 'domain', 'one.microsite.local', 'siteone@microsite.local', $settings);

    $settings = ['user_id' => $this->microSiteOwnerUser->id(), 'registered' => TRUE];
    $site_two = $this->createSite('generic', 'Site Two', 'Site two slogan', 'domain', 'two.microsite.local', 'sitetwo@microsite.local', $settings);
    $site_three = $this->createSite('generic', 'Site Three', 'Site three slogan', 'domain', 'three.microsite.local', 'sitethree@microsite.local', $settings);

    $site_one->setRegistered(TRUE);
    $site_one->save();
    $this->drupalGet(Url::fromUri($site_one->getSitePath()));
    $this->assertSession()->statusCodeEquals(403);
    $this->logInUser($this->globalAdminUser);
    $this->drupalGet(Url::fromUri($site_one->getSitePath()));
    $this->assertSession()->statusCodeEquals(200);
    $xpath = $this->xpath("//a[contains(@href, '/site/" . $site_one->id() . "/content')]");
    $this->assertEquals(count($xpath), 0, 'Tab content not found on the micro site home.');

    $this->drupalGet(Url::fromUri($this->masterUrl));
    $this->logInUser($this->globalAdminUser);
    $config_url = $this->masterUrl . '/admin/config/micro_site/node';
    $this->drupalGet(Url::fromUri($config_url));
    $this->assertSession()->statusCodeEquals(200);
    $this->checkField('node_types[page]');
    $this->pressButton('Save configuration');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    $generic->setTypes(['page']);
    $generic->save();

    $this->drupalGet(Url::fromUri($site_one->getSitePath()));
    $xpath = $this->xpath("//a[contains(@href, '/site/" . $site_one->id() . "/content')]");
    $this->assertEquals(count($xpath), 1, 'Tab content found on the micro site home.');

    $this->clickLink('Content');
    $this->assertSession()->pageTextContains('Add Basic page');
    $this->assertSession()->pageTextNotContains('Add Article');
    $xpath = $this->xpath("//a[contains(@href, '/site/" . $site_one->id() . "/add/page')]");
    $this->assertEquals(count($xpath), 1, 'Add Basic page link found.');
    $xpath = $this->xpath("//a[contains(@href, '/site/" . $site_one->id() . "/add/article')]");
    $this->assertEquals(count($xpath), 0, 'Add Article link not found.');

    $this->drupalGet(Url::fromUri($config_url));
    $this->checkField('node_types[article]');
    $this->pressButton('Save configuration');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    $this->drupalGet(Url::fromUri($site_one->getSitePath()));
    $this->clickLink('Content');
    $this->assertSession()->pageTextNotContains('Add Article');

    $generic->setTypes(['page', 'article']);
    $generic->save();
    $this->drupalGet(Url::fromUri($site_one->getSitePath()));

    $this->clickLink('Content');
    $this->assertSession()->pageTextContains('Add Article');
    $this->assertSession()->pageTextContains('Add Basic page');
    $xpath = $this->xpath("//a[contains(@href, '/site/" . $site_one->id() . "/add/article')]");
    $this->assertEquals(count($xpath), 1, 'Add Article link found.');

    $this->clickLink('Add Article');
    $target_id = $site_one->label() . ' (' . $site_one->id() . ')';
    $this->assertSession()->fieldValueEquals('site_id[0][target_id]', $target_id);
    $this->fillField('title[0][value]', 'Article 1');
    $this->pressButton('Save');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Article Article 1 has been created.');
    $articles = $this->entityTypeManager->getStorage('node')->loadByProperties(['title' => 'Article 1']);
    $this->assertEquals(count($articles), 1, 'One article found.');
    $article = reset($articles);
    $article_url = '/node/' . $article->id();
    $article_url_edit = '/node/' . $article->id() . '/edit';

    // Admin user have access to the node
    $this->drupalGet($this->masterUrl . $article_url);
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet($this->masterUrl);
    $this->clickLink('Log out');

    $this->drupalGet($this->masterUrl . $article_url);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet($site_two->getSitePath() . $article_url);
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($site_three->getSitePath() . $article_url);
    $this->assertSession()->statusCodeEquals(403);
    // Global admin is logged in on site one.
    $this->drupalGet($site_one->getSitePath() . $article_url);
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet($site_one->getSitePath());
    $this->clickLink('Log out');
    $this->drupalGet($site_one->getSitePath() . $article_url);
    $this->assertSession()->statusCodeEquals(403);

    $user = $this->createUserWithPassword(['access content', 'view own unpublished site entity', 'view published site entities', 'publish on any assigned site']);
    $this->drupalGet($site_one->getSitePath());
    $this->logInUser($user, FALSE);
    $this->drupalGet($site_one->getSitePath() . $article_url);
    $this->assertSession()->statusCodeEquals(403);

    $site_one->set(SiteUsers::MICRO_SITE_MEMBER, $user);
    $site_one->save();
    $this->drupalGet($site_one->getSitePath() . $article_url);
    $this->assertSession()->statusCodeEquals(200);

    $site_one->setPublished(TRUE);
    $site_one->save();

    $this->drupalGet($site_one->getSitePath());
    $this->clickLink('Log out');
    $this->drupalGet($site_one->getSitePath() . $article_url);
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet($site_one->getSitePath());
    $this->logInUser($user);
    $xpath = $this->xpath("//a[contains(@href, '/site/" . $site_one->id() . "/content')]");
    $this->assertEquals(count($xpath), 0, 'Tab content not accessible by member.');
    $site_one->set(SiteUsers::MICRO_SITE_CONTRIBUTOR, $user);
    $site_one->set(SiteUsers::MICRO_SITE_MEMBER, []);
    $site_one->save();

    $this->drupalGet($site_one->getSitePath());
    $xpath = $this->xpath("//a[contains(@href, '/site/" . $site_one->id() . "/content')]");
    $this->assertEquals(count($xpath), 1, 'Tab content accessible by contributor.');
    $this->clickLink('Content');
    $this->assertSession()->pageTextContains('Article 1');
    $xpath = $this->xpath("//a[contains(@href, '/node/" . $article->id() . "/edit')]");
    $this->assertEquals(count($xpath), 0, 'Article not editable by member.');

    $this->drupalGet($site_one->getSitePath() . $article_url_edit);
    $this->assertSession()->statusCodeEquals(403);

    $site_one->set(SiteUsers::MICRO_SITE_MANAGER, [$user]);
    $site_one->set(SiteUsers::MICRO_SITE_CONTRIBUTOR, []);
    $site_one->save();

    // @TODO check the user cache tags is well invalidated. Its cache tags is
    // invalidated in the site postSave() method. We need here to invalidate it
    // yet otherwise the test fails.
    Cache::invalidateTags($user->getCacheTags());

    // Tests the publish on master checkbox is present or not.
    $this->drupalGet($site_one->getSitePath());
    $this->clickLink('Content');
    $xpath = $this->xpath("//a[contains(@href, '/node/" . $article->id() . "/edit')]");
    $this->assertEquals(count($xpath), 1, 'Article editable by manager.');
    $this->drupalGet($site_one->getSitePath() . $article_url_edit);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Others sites');
    $this->assertSession()->pageTextContains('Disable main site canonical url');
    $this->assertSession()->pageTextNotContains('Publish on master');

    $this->drupalGet($site_one->getSitePath());
    $this->clickLink('Log out');
    $new_user = $this->createUserWithPassword(['access content', 'view own unpublished site entity', 'view published site entities', 'publish on any assigned site', 'publish on master']);
    $site_one->set(SiteUsers::MICRO_SITE_MANAGER, [$new_user]);
    $site_one->save();
    $this->logInUser($new_user);
    $this->drupalGet($site_one->getSitePath() . $article_url_edit);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Others sites');
    $this->assertSession()->pageTextContains('Disable main site canonical url');
    $this->assertSession()->pageTextContains('Publish on master');
  }

}
